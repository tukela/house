package com.five.mapper;

import com.five.domain.House;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface HouseMapper {

    //后端页面====================================================

    //查询房屋
    List<House> queryAllHouse(House house);

    //添加房屋
    void alter();
    int addHouse(House house);

    //修改房屋
    int updateHouse(House house);

    //取消房屋
    int cancelHouse(House house);

    //查询房屋数量
    Long queryHouseNumber(@Param("status")Integer status);

    //查询房屋参数(App与后端共用)
    House queryHouseById(House house);


    //小程序====================================================

    //查询房屋(App)
    List<House> queryHouse(House house);

    //最新房屋
    List<House> queryHouseByNew(House house);








}
