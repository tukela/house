package com.five.mapper;

import com.five.domain.Decoration;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;

@Mapper
public interface DecorationMapper {

    //查询装修风格
    List<Decoration> queryAllDecoration(Decoration decoration);

    //导航栏查询
    List<Decoration> queryDecorationByUse(Decoration decoration);

    //根据style查询
    Decoration queryDecorationByStyle(@Param("style") String style);

    //添加装修风格
    void alter();
    int addDecoration(Decoration decoration);

    //修改装修风格信息
    int updateDecoration(Decoration decoration);

    //删除装修风格信息
    int deleteDecoration(Decoration decoration);
}
